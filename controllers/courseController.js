const Course = require("../models/course.js");
const User = require("../models/user.js");

// [CODE DISCUSSION]
/*module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((newCourse, error) => {
		if(error){
			return false;
		}
		else {
			return true;
		}
	})
}*/

// s39-40 (Creating a course)
module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            return newCourse.save().then((course, error) => {
      
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        }
        
    })
}

module.exports.getAllCourse = () => {
	return Course.find({/*document*/}).then(result => {
		return result
	})
}

// GET all Active Courses
module.exports.getActiveCourses = () => {

	return Course.find({isActive:true}).then( result => {
		return result;
	})

}

// GET specific course
module.exports.getCourse = (courseId) => {
						// inside the parenthesis should be the id
	return Course.findById(courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){

		//update code
		return Course.findByIdAndUpdate(courseId, {
			name: newData.course.name,
			description: newData.course.description,
			price: newData.course.price
		}).then((updatedCourse, error) => {
			if(error){
				return false
			}
			return updatedCourse
		})

	} else {
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

// s39-40 (Archiving a course)
module.exports.archiveCourse = (courseInfo, reqBody) => {

	return Course.findById(courseInfo.courseId).then(result => {

		if(courseInfo.isAdmin == true) {

			let updateActiveField = {
				isActive: reqBody.isActive
			}

			return Course.findByIdAndUpdate(result._id, updateActiveField).then((course,err) => {

				// Course is not archived
				if(err) {
					console.log("error bai")
					return false;

				// Course is archived successfully
				} else {

					return true;
				}
			})

		} else {

			//If the user is not an Admin
			console.log("dili admin bai")
			return false
		}

	})
}