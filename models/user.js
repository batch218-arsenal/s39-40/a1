const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String, 
		required: [true, "Mobile Number is required."]
	},
	/*
		- enrollments - Array of objects
		    - courseId - String
		    - enrolledOn - Date (Default value - new Date object)
		    - status - String (Default value - Enrolled)
    */
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is required."]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled."
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);

// Sample output in database
/*
	{
		"firstName" : "Christopher",
		"lastName" : "Malinao",
		"enrollments" : [
			{
				"courseId" : "crs001",
				"enrolledOn" : "July 1, 2022",
				"status" : "Enrolled"
			},
			{
				"courseId" : "crs002",
				"enrolledOn" : "July 1, 2022",
				"status" : "Enrolled"
			}

		]
	}
*/

